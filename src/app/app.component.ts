import { Component, OnInit } from '@angular/core';
import { AssetClassService } from 'src/Services/AssetClass/asset-class.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AssetClassModel, BestInClass, Description } from 'src/Models/AssetClass.model';
import { Strumento } from 'src/Models/Common.model';
import { MatTableDataSource } from '@angular/material/table';
import { LanguageService } from 'src/Services/Language Pack/language.service';
import { DataExchangeService } from 'src/Services/Data Exchange/data-exchange.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  
{ 
  loading : boolean; allStrum : string[] = []; 
  allAC : AssetClassModel[] = []; BiCList : BestInClass[] = []; selected : BestInClass = new BestInClass(); hide : boolean = true; 
  titoli : Strumento[] = []; isSelected : boolean = false; tblElem; note : string = ""; acID : number = 0;
  bicDate : string = "";
  columns : string[] = ["Status","NAME","BFC_CAT_NAME","RATING_IMG","RISK_GRADE","SHARPE_3Y_EUR","PERF_1Y_EUR","PERF_3Y_EUR","STD_DEV_3Y_EUR"];

  constructor(public lang : LanguageService, private acService : AssetClassService, private snackBar : MatSnackBar, private dataEX : DataExchangeService)
  {
    this.loading = true;

    this.acService.ListAssetClass().subscribe((list)=>
    {
      if(list != null)
      {
        this.allAC = list.filter(it=>it.Type != -10);
        this.getAllStrums();

        this.acService.InfoStrumenti(this.allStrum).subscribe((strums)=>
        {
          if(strums != null)
          {
            this.createStrumentsInfo(strums);
            this.getFirstElement();
            this.loading = false;
          }
          else
          {
            this.loading = false;
            this.openSnackBar(this.lang.getDictionary("Attenzione"), this.lang.getDictionary("MsgKO"));  
          }
        },
        (err: HttpErrorResponse) => 
        {
          this.loading = false;
          if (err.error instanceof Error) this.openSnackBar(this.lang.getDictionary("Attenzione"), this.lang.getDictionary("MsgKO") + " " + err.status);
          else this.openSnackBar(this.lang.getDictionary("Attenzione"), this.lang.getDictionary("MsgKO") + " " + err.status);     
        });
      }
      else 
      {
        this.loading = false;
        this.openSnackBar(this.lang.getDictionary("Attenzione"), this.lang.getDictionary("MsgKO")); 
      }
    },
    (err: HttpErrorResponse) => 
    {
      this.loading = false;
      if (err.error instanceof Error) this.openSnackBar(this.lang.getDictionary("Attenzione"), this.lang.getDictionary("MsgKO") + " " + err.status);
      else this.openSnackBar(this.lang.getDictionary("Attenzione"), this.lang.getDictionary("MsgKO") + " " + err.status);     
    });
  }

  formatDate(dt : Date) { dt = new Date(dt); return dt.toLocaleDateString(this.dataEX.locale); }

  getFirstElement()
  {
    if(this.allAC.length>0)
    {
      this.acID = this.allAC[0].ID;
      this.selectedAC(this.allAC[0]);

      if(this.allAC[0].BestInClass.length > 0)
      {
        this.bicDate = this.formatDate(this.allAC[0].BestInClass[0].Date);
        this.selectedBiC(this.allAC[0].BestInClass[0]);
      }
    }
  }

  getDetailUrl(fcode : string) { return this.dataEX.fws + "/Products/NGProduct#/" + fcode; }

  openSnackBar(message: string, action: string) { this.snackBar.open(message, action, { duration: 4000, }); }

  ngOnInit() { }

  getACName(elem : Description[]) 
  {
    let found = elem.find(it=> it.Language == this.dataEX.language);
    if(found != undefined) return found.Name;
    else 
    {
      found = elem.find(it=>it.Name!= null && it.Name != "");
      if(found != undefined) return found.Name;
      else return "-";
    }
  }

  getAllStrums() { this.allAC.forEach((ac)=> { ac.BestInClass.forEach((bic)=> { this.allStrum = this.allStrum.concat(bic.FidaCodes); }); }); }

  createStrumentsInfo(list : any[])
  {
    list.forEach((elem)=>
    {
      let row : Strumento = new Strumento();
      for (const key in elem) 
      {
        if(elem.hasOwnProperty(key)) 
        {
          switch(key)
          {
            case "NAME" : row.NAME = elem[key]; break;
            case "ISIN_CODE" : row.ISIN_CODE = elem[key] || "-"; break;
            case "FIDA_CODE" : row.FIDA_CODE = elem[key]; break;
            case "BFC_CAT_NAME" : row.BFC_CAT_NAME = elem[key] || "-"; break;
            case "BFC_RATING_36M" : row.BFC_RATING_36M = elem[key] != null ? elem[key] : 0; break;
            case "PERF_1Y_EUR" : row.PERF_1Y_EUR = elem[key] != null ? (elem[key]*100).toLocaleString(this.dataEX.locale,{ minimumFractionDigits : 2, maximumFractionDigits : 2 })+" %" : "-"; break;
            case "PERF_3Y_EUR" : row.PERF_3Y_EUR =elem[key] != null ? (elem[key]*100).toLocaleString(this.dataEX.locale,{ minimumFractionDigits : 2, maximumFractionDigits : 2 })+" %" : "-"; break;
            case "STD_DEV_3Y_EUR" : row.STD_DEV_3Y_EUR = elem[key] != null ? (elem[key]*100).toLocaleString(this.dataEX.locale,{ minimumFractionDigits : 2, maximumFractionDigits : 2 })+" %" : "-"; break;
            case "RISK_GRADE" : row.RISK_GRADE = elem[key] != null ? elem[key] : "-"; break;
            case "SHARPE_3Y_EUR" : row.SHARPE_3Y_EUR = elem[key] != null ? elem[key].toLocaleString(this.dataEX.locale,{ minimumFractionDigits : 2, maximumFractionDigits : 2 }) : "-"; break;
          }
        }
      }
      this.titoli.push(row);
    });
  }

  selectedAC(elem : AssetClassModel) 
  {
    this.hide = true; 
    this.isSelected = true; 
    this.bicDate = "";

    let found = elem.Description.find(it=> it.Language == this.dataEX.language);
    if(found != undefined) this.note = found.Description;
    else 
    {
      found = elem.Description.find(it=>it.Description!= null && it.Description != "");
      if(found != undefined) this.note = found.Description;
    }

    this.BiCList = elem.BestInClass; 

    this.BiCList.sort((a,b)=> 
    { 
      if(typeof b.Date != "object") b.Date = new Date(b.Date);
      if(typeof a.Date != "object") a.Date = new Date(a.Date);
      return b.Date.getTime() - a.Date.getTime(); 
    });

    if(this.BiCList.length>0)
    {
      this.bicDate = this.formatDate(this.BiCList[0].Date);
      this.selectedBiC(this.BiCList[0]);
    }
  }

  selectedBiC(elem : BestInClass) 
  { 
    this.selected = elem;
    let toShow : Strumento[] = [];
    let previous : BestInClass = new BestInClass();

    this.selected.FidaCodes.forEach((code)=> { toShow.push(this.titoli.find(it=>it.FIDA_CODE == code)); });
    toShow.forEach((elem)=> { elem.Status = 1; });

    this.BiCList.forEach((elem,index)=> 
    { 
      if(typeof elem.Date != "object") elem.Date = new Date(elem.Date);
      if(typeof this.selected.Date != "object") this.selected.Date = new Date(this.selected.Date);    
      if((elem.Date.toLocaleDateString(this.dataEX.locale) == this.selected.Date.toLocaleDateString(this.dataEX.locale)) && index<this.BiCList.length) previous = this.BiCList[index+1];
    });     
    
    if(previous != undefined)
    {
      let oldStruments : Strumento[] = [];
      previous.FidaCodes.forEach((old)=>
      {
        let oldStrum = this.titoli.find(el => el.FIDA_CODE == old);
        oldStrum.Status = -1;
        oldStruments.push(oldStrum);
      });

      toShow.forEach((elem)=>
      {
        if(oldStruments.find(it=> it.FIDA_CODE == elem.FIDA_CODE))
        {
          elem.Status = 0;
          oldStruments = oldStruments.filter(it=> it.FIDA_CODE != elem.FIDA_CODE);
        }
      });

      if(oldStruments.length>0) toShow = toShow.concat(oldStruments);   
    }

    toShow.sort((a,b)=> { return b.Status - a.Status; });
    this.tblElem = new MatTableDataSource(toShow);
    this.hide = false; 
  }

  openGuida() { this.dataEX.showGuide(this.lang.getDictionary("GuidaTitolo"),this.lang.getDictionary("BiCGuide")); }
}
