import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'ratingpipe'
})
export class RatingPipe implements PipeTransform 
{
    constructor(private sanitizer: DomSanitizer) {}

    transform(value: number, args: {[key: string]: any}) 
    {
        let fidaIcons: string = "";
        if(value > 0)
        {
            for(var i=0; i < value; i++)
            {
                fidaIcons += "<i class='" + args['type'] + "'style='margin-right: 2px;'></i>";
            }
        }
        return this.sanitizer.bypassSecurityTrustHtml(fidaIcons);
    }
}