import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeIT from '@angular/common/locales/it';
registerLocaleData(localeIT);

import { AppComponent } from './app.component';
import { MaterialModule } from './import.material';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogGuidaComponent } from './dialog-guida/dialog-guida.component';
import { RatingPipe } from './rating-pipe';

@NgModule({
    declarations: [
        AppComponent,
        DialogGuidaComponent,
        RatingPipe
    ],
    imports: [
        BrowserModule,
        MaterialModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [
        { provide: LOCALE_ID, useValue: 'it' },
        { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
        { provide: MAT_DATE_LOCALE, useValue: 'it' } // moment datapicker
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
