import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LanguageService } from 'src/Services/Language Pack/language.service';

@Component({
  selector: 'app-dialog-guida',
  templateUrl: './dialog-guida.component.html',
  styleUrls: ['./dialog-guida.component.css']
})
export class DialogGuidaComponent implements OnInit 
{
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public lang : LanguageService) { }

  ngOnInit() { }
}
