import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogGuidaComponent } from './dialog-guida.component';

describe('DialogGuidaComponent', () => {
  let component: DialogGuidaComponent;
  let fixture: ComponentFixture<DialogGuidaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogGuidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogGuidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
