export class AssetClassModel
{
    Benchmark: Benchmark[];
    BestInClass: BestInClass[];
    Code: string;
    Definition: Definition[]; //Categorie
    Description: Description[]; //Dati anagrafici
    ID: number;
    Type: number;
    presentInModelPortfolio: number;

    constructor()
    {
        this.Benchmark = [];
        this.BestInClass = [];
        this.Code = "";
        this.Definition = [];
        this.Description = [];
        this.ID = -1;
        this.Type = 0; // -10 = LIQ
        this.presentInModelPortfolio = null;
    }
}

export class Benchmark    
{
    Date: Date;
    Description: string;
    FidaCode: string;
    ID: number;
    Order: number;
    Type: string;

    constructor()
    {    
        this.Description = "";
        this.FidaCode = "";
        this.ID = -1;
        this.Order = -1;
        this.Type = "";
        this.Date = new Date();
    }
}

export class BestInClass
{
    Date: Date;
    Description: string;
    FidaCodes: string[];
    Titoli : Titoli[]; //Serve solo in Front End per anagrafiche titoli
    ID: number;
    Order: number;
    Type: string;
    isNew : boolean; //Serve solo in Front End per verificare se elemento nuovo
    isSelected : boolean;

    constructor()
    {
        this.Date = new Date();
        this.isNew = true;
        this.isSelected = false;
        this.Titoli = [];
        this.FidaCodes = [];
        this.Description = "";
        this.ID = -1;
        this.Order = -1;
        this.Type = ""; 
    }
}

export class Definition
{
    AssetClassCode: string;
    ID: number;
    Tipo: number;
    Value: string;
    Name : string; //Solo in Front End
    
    constructor()
    {
        this.AssetClassCode = null;
        this.ID = null;
        this.Tipo = null;
        this.Value = "";
        this.Name = "";
    }
}

export class Description
{
    Macro: string;
    Description: string;
    Name: string;
    Language: string;
}

export class Titoli
{
    Nome : string;
    ISIN : string;
    FidaCode : string;
}

export class AssetClassEdit
{
    Description : DescriptionEdit[];
    ID : number;
    Type : number;
    Benchmark : Benchmark[];
    Definition : Definition[];

    constructor()
    {
        this.Description = [];
        this.ID = null;
        this.Type = null;
        this.Benchmark = [];
        this.Definition = [];
    }
}

export class DescriptionEdit
{
    Language : string;
    Macro : string;
    Description : string;
    Name : string;
}

export class BestInClassToSave
{
    AssetClassID: number;
    Date: Date;
    FidaCodes: string[];
  }