export class Strumento
{
    Status : number;
    NAME : string;
    ISIN_CODE : string;
    FIDA_CODE : string;
    BFC_CAT_NAME : string;
    BFC_RATING_36M : number;
    RATING_IMG : string;
    PERF_1Y_EUR : string;
    PERF_3Y_EUR : string;
    STD_DEV_3Y_EUR : string;
    RISK_GRADE : string;
    SHARPE_3Y_EUR : string;

    constructor()
    {
        this.Status = 1; // 0 = Invariato , -1 = Eliminato , 1 = Aggiunto
        this.NAME = "" ;
        this.ISIN_CODE = "" ;
        this.FIDA_CODE = "" ;
        this.BFC_CAT_NAME = "" ;
        this.BFC_RATING_36M = -1 ;
        this.RATING_IMG = "";
        this.PERF_1Y_EUR = "" ; 
        this.PERF_3Y_EUR = "" ; 
        this.STD_DEV_3Y_EUR = "" ;
        this.RISK_GRADE = "" ;
        this.SHARPE_3Y_EUR = "" ; 
    }
}