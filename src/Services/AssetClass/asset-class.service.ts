import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AssetClassModel } from 'src/Models/AssetClass.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AssetClassService 
{
  user = window["SessionData"].userid;
  apiUrl = window['SessionData'].webapiurl;
  routing = 'api/modelportfolios';
  search = 'api/search';

  constructor(private http: HttpClient) { }

  ListAssetClass(): Observable<AssetClassModel[]> 
  {
    const getUrl = this.apiUrl + '/' + this.routing + '/' + this.user + '/ListAssetClass';
    return this.http.get<AssetClassModel[]>(getUrl);
  }

  InfoStrumenti(codes : string[]) : Observable<any>
  {
    const getUrl = this.apiUrl + '/' + this.search +'/' + this.user + '/GetData' ;
    let request = { "FidaCodes" : codes , "Fields" : ["NAME","ISIN_CODE","FIDA_CODE","BFC_CAT_NAME","BFC_RATING_36M","PERF_1Y_EUR","PERF_3Y_EUR","STD_DEV_3Y_EUR","RISK_GRADE","SHARPE_3Y_EUR"] };
    return this.http.post<any>(getUrl,request);
  }
}
