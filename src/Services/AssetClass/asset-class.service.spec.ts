import { TestBed } from '@angular/core/testing';

import { AssetClassService } from './asset-class.service';

describe('AssetClassService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AssetClassService = TestBed.get(AssetClassService);
    expect(service).toBeTruthy();
  });
});
