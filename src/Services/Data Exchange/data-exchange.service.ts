import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogGuidaComponent } from 'src/app/dialog-guida/dialog-guida.component';

@Injectable({
  providedIn: 'root'
})
export class DataExchangeService 
{
  language = window["SessionData"].lang; fws = window["SessionData"].workstation;
  locale : string = "";

  constructor(private dialog : MatDialog) 
  { 
    switch(this.language)
    {
      case "it" : this.locale = "it-IT"; break;
      case "en" : this.locale = "en-US"; break;
      case "fr" : this.locale = "it-IT"; break;
    }
  }

  showGuide(titolo : string , messaggio : string) { this.dialog.open(DialogGuidaComponent, { data:{ title : titolo , message : messaggio } , width : "35%" }); }
}
