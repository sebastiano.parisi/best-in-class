import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageService 
{
  lang = window["SessionData"].lang;

  Dictionary=
  {
    "Chiudi" : { "it" : "Chiudi" , "en" : "Close" , "fr" : "" },
    "BiCGuide" : { "it" : "Le Buying list suggeriscono per ogni asset class e per ogni data gli strumenti o lista di strumenti consigliati per replicarle e corredate da una serie di misure statistiche." , "en" : "Buying lists suggest for each asset class and for each date tools or list of them recommended to replicate the asset class and accompanied by a series of statistical data." , "fr" : "" },
    "GuidaTitolo" : { "it" : "Guida" , "en" : "Guide" , "fr" : "" },
    "Guida" : { "it" : "Clicca per aprire la guida contestuale." , "en" : "Click to open contextual guide." , "fr" : "" },
    "SelectAC" : { "it" : "Selezionare Asset Class" , "en" : "Select Asset Class", "fr" : "" },
    "SelectDate" : { "it" : "Selezionare Data" , "en" : "Select Date", "fr" : "" },
    "NoBiC" : { "it" : "Non è presente alcuna Best in Class in questa Asset Class", "en" : "No Best in Class to display for this Asset Class." , "fr" : "" },
    "Attenzione" : { "it" : "ATTENZIONE" , "en" : "WARNING" , "fr" : "" },
    "MsgOK" : { "it" : "Operazione effettuata con successo." , "en" : "Operation successful." , "fr" : "" },
    "MsgKO" : { "it" : "Non è stato possibile effettuare l'operazione." , "en" : "Something went wrong with operation." , "fr" : "" },
    "Titoli" : { "it" : "TITOLI" , "en" : "FUNDS" , "fr" : "" },
    "Added" : { "it" : "Fondo aggiunto" , "en" : "Added fund" , "fr" : "" },
    "Equal" : { "it" : "Fondo invariato" , "en" : "Unchanged fund" , "fr" : "" },
    "Removed" : { "it" : "Fondo rimosso" , "en" : "Removed fund" , "fr" : "" },
    "Note" : { "it" : "NOTE" , "en" : "NOTES" , "fr" : "" },
    "NoStrum" : { "it" : "Non è presente alcuno strumento in questa Best in Class" , "en" : "No fund to display for this Best in Class" , "fr" : "" },
    "Nome" : { "it" : "Nome" , "en" : "Name" , "fr" : "" },
    "FidaCat" : { "it" : "Categorie FIDA" , "en" : "FIDA Category" , "fr" : "" },
    "Risk" : { "it" : "Grado di rischio (SRRI)" , "en" : "Risk grade (SRRI)", "fr" : "" },
    "Sharpe" : { "it" : "Sharpe a 3 anni" , "en" : "3 years Sharpe" , "fr" : "" },
    "Perf1" : { "it" : "Performance 1 anno" , "en" : "1 year Performance" , "fr" : "" },
    "Perf3" : { "it" : "Performance 3 anni" , "en" : "3 years Performance" , "fr" : "" },
    "Vol3" : { "it" : "Volatilità a 3 anni" , "en" : "3 years Volatility" , "fr" : "" },
  }

  // "" : { "it" : "" , "en" : "" , "fr" : "" },

  constructor() 
  { 
    if(this.lang == "fr") this.lang == "en";
  }

  getDictionary(par : string) { return this.Dictionary[par][this.lang]; }
}
